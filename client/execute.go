package client

import (
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"codeberg.org/dralmaember/tldmsg"
)

const (
	mtypePost      = 1
	mtypeRead      = 2
	mtypeHead      = 3
	mtypeGet       = 4
	mtypeComposite = 5

	mtypeServerSuccess = 0
	mtypeServerFailure = 1
)

func (self *ProtoStream) executeReq(req *request) error {
	switch req.typ {
	case postReq:
		return self.post(req.data.(postReqData))

	case readReq:
		return self.read(req.data.(readReqData))

	case headReq:
		return self.head(req.data.(headReqData))

	case getReq:
		return self.get(req.data.(getReqData))

	case compReq:
		return self.composite(req.data.(compReqData))
	default:
		log.Panicln("Encountered invalid request type:", req.typ)
	}

	return nil
}

func (self *ProtoStream) post(data postReqData) error {
	headerSection := ""

	for name, val := range data.headers {
		headerSection += fmt.Sprintf("%s: %s\r\n", name, val)
	}
	headerSection += "\r\n"

	contents := make([]byte, base64.StdEncoding.EncodedLen(len([]byte(data.post))))
	base64.StdEncoding.Encode(contents, []byte(data.post))

	fullMsg := make([]byte, 0)
	fullMsg = append(fullMsg, []byte(headerSection)...)
	fullMsg = append(fullMsg, contents...)
	err := self.conn.SendMessage(tldmsg.Message{MType: mtypePost, Content: fullMsg})

	res, err := self.conn.Receive()
	if err != nil {
		return err
	}

	if res.MType != mtypeServerSuccess {
		if res.MType == mtypeServerFailure {
			return errors.New(string(res.Content))
		} else {
			return errors.New("Invalid server response")
		}
	}

	return err
}

func (self *ProtoStream) read(data readReqData) error {
	reqMsg := tldmsg.Message{
		MType:   mtypeRead,
		Content: []byte(data.group),
	}
	err := self.conn.SendMessage(reqMsg)
	if err != nil {
		return err
	}

	msg, err := self.conn.Receive()
	if err != nil {
		return err
	}

	if msg.MType != mtypeServerSuccess {
		if msg.MType == mtypeServerFailure {
			return fmt.Errorf("Server returned error: %s", string(msg.Content))
		} else {
			return errors.New("Server returned invalid response")
		}
	}

	result := strings.Split(string(msg.Content), "\r\n")
	*data.out = &result
	return nil
}

func (self *ProtoStream) head(data headReqData) error {
	reqMsg := tldmsg.Message{
		MType:   mtypeHead,
		Content: []byte(data.group + " " + data.id),
	}

	err := self.conn.SendMessage(reqMsg)
	if err != nil {
		return err
	}

	resp, err := self.conn.Receive()
	if err != nil {
		return err
	}
	if resp.MType != mtypeServerSuccess {
		if resp.MType != mtypeServerFailure {
			return errors.New("Server returned invalid reply")
		} else {
			return errors.New("Server returned failure")
		}
	}

	out := make(map[string]string)
	splitted := strings.Split(string(resp.Content), "\r\n")
	for _, header := range splitted {
		sep := strings.SplitN(header, ":", 2)
		if len(sep) < 2 {
			return errors.New("Server responed with invalid header")
		}

		sep[1] = strings.Trim(sep[1], " \t")
		out[sep[0]] = sep[1]
	}

	*data.out = out

	return nil
}

func (self *ProtoStream) get(data getReqData) error {
	reqStr := data.group + " " + data.id
	reqMsg := tldmsg.Message{MType: mtypeGet, Content: []byte(reqStr)}

	err := self.conn.SendMessage(reqMsg)
	if err != nil {
		return err
	}

	resp, err := self.conn.Receive()
	if err != nil {
		return err
	}

	if resp.MType != mtypeServerSuccess {
		if resp.MType == mtypeServerFailure {
			return fmt.Errorf("Request failed on server: %s", string(resp.Content))
		} else {
			// I'm getting pretty fucking tired of validating everything, y'know.
			// I probably missed something. I don't give a crap anymore.
			return errors.New("The server isn't working and returned a bad mtype. Too bad.")
		}
	}

	res := string(resp.Content)
	split := strings.SplitN(res, "\r\n\r\n", 2)
	if len(split) != 2 {
		return errors.New("The server returned an invalid message")
	}

	headSec, contSec := split[0], split[1]
	data.out.Headers = make(map[string]string)
	for _, line := range strings.Split(headSec, "\r\n") {
		hsplit := strings.SplitN(line, ":", 2)
		if len(hsplit) != 2 {
			return errors.New("The server returned an invalid header. GREAT!")
		}

		name, value := hsplit[0], strings.Trim(hsplit[1], " \t")
		data.out.Headers[name] = value
	}

	data.out.Contents = make([]byte, base64.StdEncoding.DecodedLen(len([]byte(contSec))))
	_, err = base64.StdEncoding.Decode(data.out.Contents, []byte(contSec))
	if err != nil {
		return err
	}

	return nil
}

func (self *ProtoStream) composite(data compReqData) error {
	reqCont := data.group + " " +
		fmt.Sprintf("%d %d %d", data.minTime.Unix(), data.maxTime.Unix(), data.limit)
	req := tldmsg.Message{
		MType:   mtypeComposite,
		Content: []byte(reqCont),
	}

	err := self.conn.SendMessage(req)
	if err != nil {
		return err
	}

	resp, err := self.conn.Receive()
	if err != nil {
		return err
	}

	if resp.MType != mtypeServerSuccess {
		if resp.MType == mtypeServerFailure {
			return fmt.Errorf("Request failed on server: %s", string(resp.Content))
		} else {
			return errors.New("Server returned invalid response")
		}
	}

	splitted := strings.Split(string(resp.Content), "\r\n\r\n")
	if len(splitted) == 1 {
		// At least one element must always exist, since we're splitting a string.
		if splitted[0] == "" {
			// Valid, emtpy response
			return nil
		} else {
			// Invalid, the response must always end with
			// CRLFCRLF (and therefore an empty field *must* exist)
			return errors.New("Server composite response invalid; doesn't end with CRLFCRLF")
		}
	}

	if splitted[len(splitted)-1] != "" {
		return errors.New("Server composite response invalid; doesn't end with CRLFCRFL")
	}
	splitted = splitted[0 : len(splitted)-1]

	for _, entry := range splitted {
		splittedEntry := strings.Split(entry, "\r\n")
		if len(splittedEntry) != 6 {
			// There must *always* be exactly six fields
			return fmt.Errorf("Invalid composite response entry; wrong number of fields (expected 6, got %d instead)",
				len(splittedEntry))
		}

		var timeOfPostN int64
		n, err := fmt.Sscanf(splittedEntry[1], "%d", &timeOfPostN)
		if err != nil || n != 1 {
			return errors.New("Invalid date in response")
		}
		timeOfPost := time.Unix(timeOfPostN, 0)

		entryStruct := CompositeField{}
		entryStruct.Id = splittedEntry[0]
		entryStruct.DatePosted = timeOfPost
		entryStruct.IdInReplyTo = splittedEntry[2]
		entryStruct.ClientId = splittedEntry[3]
		entryStruct.From = splittedEntry[4]
		entryStruct.Subject = splittedEntry[5]

		if entryStruct.IdInReplyTo == "-" {
			entryStruct.IdInReplyTo = ""
		}

		*data.out = append(*data.out, entryStruct)
	}

	return nil
}
