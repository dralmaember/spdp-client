package client

import (
	"crypto/tls"
	"errors"

	"codeberg.org/dralmaember/tldmsg"
)

type Conn struct {
	Conn *tls.Conn
	cert tls.Certificate
	conf tls.Config
}

func NewConn(cert tls.Certificate) *Conn {
	var conn Conn

	conn.cert = cert
	conn.conf = tls.Config{Certificates: []tls.Certificate{cert}}

	return &conn
}

func (self *Conn) Connect(host string) error {
	conn, err := tls.Dial("tcp", host, &self.conf)
	self.Conn = conn
	if err != nil {
		return err
	}

	return nil
}

func (self *Conn) Close() error {
	if self.Conn == nil {
		return errors.New("No connection to close")
	}

	err := self.Conn.Close()
	self.Conn = nil
	return err
}

func (self *Conn) SendMessage(msg tldmsg.Message) error {
	return msg.WriteToStream(self.Conn)
}

func (self *Conn) Receive() (msg tldmsg.Message, err error) {
	msg, err = tldmsg.ReadFromStream(self.Conn)
	return
}
