package client

import "time"

type Post struct {
	Headers  map[string]string
	Contents []byte
}

type ProtoStream struct {
	conn *Conn
	reqs []*request
}

type reqType uint8

type request struct {
	typ  reqType
	data interface{}
}

type postReqData struct {
	headers map[string]string
	post    string
}

type readReqData struct {
	group string
	out   **[]string
}

type headReqData struct {
	group string
	id    string
	out   *map[string]string
}

type getReqData struct {
	group string
	id    string
	out   *Post
}

type compReqData struct {
	group            string
	minTime, maxTime time.Time
	limit            uint64
	out              *[]CompositeField
}

type CompositeField struct {
	Id          string
	Group       string
	DatePosted  time.Time
	IdInReplyTo string
	ClientId    string
	From        string
	Subject     string
}

const (
	postReq reqType = iota
	readReq
	headReq
	getReq
	compReq
)

func (conn *Conn) AsProtoStream() *ProtoStream {
	return &ProtoStream{conn: conn, reqs: []*request{}}
}

func (self *ProtoStream) Post(headers map[string]string, post string) *ProtoStream {
	data := postReqData{headers, post}
	self.reqs = append(self.reqs, &request{typ: postReq, data: data})
	return self
}

func (self *ProtoStream) Read(group string, out **[]string) *ProtoStream {
	data := readReqData{out: out, group: group}
	self.reqs = append(self.reqs, &request{typ: readReq, data: data})
	return self
}

func (self *ProtoStream) Head(group, id string, out *map[string]string) *ProtoStream {
	data := headReqData{out: out, group: group, id: id}
	self.reqs = append(self.reqs, &request{typ: headReq, data: data})
	return self
}

func (self *ProtoStream) Get(group, id string, out *Post) *ProtoStream {
	data := getReqData{out: out, group: group, id: id}
	self.reqs = append(self.reqs, &request{typ: getReq, data: data})
	return self
}

func (self *ProtoStream) Composite(group string, minTime, maxTime time.Time, limit uint64, out *[]CompositeField) *ProtoStream {
	data := compReqData{
		group:   group,
		minTime: minTime,
		maxTime: maxTime,
		limit:   limit,
		out:     out,
	}
	self.reqs = append(self.reqs, &request{typ: compReq, data: data})
	return self
}

func (self *ProtoStream) Execute() error {
	for _, req := range self.reqs {
		err := self.executeReq(req)
		if err != nil {
			return err
		}
	}

	return nil
}
