package main

import (
	"bufio"
	"crypto/tls"
	"fmt"
	"os"
	"strconv"
	str "strings"
	"time"

	"codeberg.org/dralmaember/spdp-client/client"
	"codeberg.org/dralmaember/tldmsg"
)

var commands = map[string]func([]string){
	"quit": func(_ []string) {
		fmt.Println("Bye!")
		os.Exit(0)
	},
	"connect": func(args []string) {
		fmt.Println(args)
		if len(args) < 1 {
			fmt.Println("Not enough arguments!")
			return
		}
		fmt.Println("Connecting to", args[0])
		err := state.conn.Connect(args[0])
		if err != nil {
			fmt.Println("Error occurred while attempting to connect:", err.Error())
		}
	},
	"close": func(_ []string) {
		if err := state.conn.Close(); err != nil {
			fmt.Println("Error:", err.Error())
		}
	},
	"send": func(args []string) {
		if len(args) < 2 {
			fmt.Println("Not enough arguments")
			return
		}

		mtype, err := strconv.Atoi(args[0])
		if err != nil {
			fmt.Println("Invalid argument!")
			return
		}
		msg := tldmsg.Message{
			MType:   uint8(mtype),
			Content: []byte(str.Join(args[1:], " ")),
		}

		err = state.conn.SendMessage(msg)
		if err != nil {
			fmt.Println("Failed to send message:", err.Error())
		}
	},
	"read": func(_ []string) {
		msg, err := state.conn.Receive()
		if err != nil {
			fmt.Println("An error happened during reading:", err.Error())
		}
		fmt.Printf("Type: %d\nContents:\n%s\n", msg.MType, msg.Content)
	},
	"post": func(_ []string) {
		headers := make(map[string]string)
		scanner := bufio.NewScanner(os.Stdin)
		for {
			fmt.Print("Header> ")
			scanner.Scan()
			line := scanner.Text()

			if line == "" {
				break
			}

			header := str.SplitN(line, ":", 2)
			if len(header) < 2 {
				fmt.Println("Invalid header!")
				continue
			}
			headers[header[0]] = header[1]
		}

		post := ""
		for {
			scanner.Scan()
			line := scanner.Text()
			if line == "." {
				break
			}
			post += line + "\n"
		}

		err := state.conn.AsProtoStream().Post(headers, post).Execute()
		if err != nil {
			fmt.Println("Error:", err.Error())
		}
	},
	"gread": func(args []string) {
		if len(args) < 1 {
			fmt.Println("Not enough arguments!")
			return
		}
		gname := args[0]
		var msgs *[]string
		err := state.conn.AsProtoStream().
			Read(gname, &msgs).
			Execute()
		if err != nil {
			fmt.Println("Failed to get a list of messages.")
			return
		}

		for _, id := range *msgs {
			fmt.Println(id)
		}
	},
	"head": func(args []string) {
		if len(args) < 2 {
			fmt.Println("Too few arguments!")
			return
		}
		grp := args[0]
		id := args[1]
		var head map[string]string
		err := state.conn.AsProtoStream().
			Head(grp, id, &head).
			Execute()

		if err != nil {
			fmt.Println("Error:", err.Error())
		}
		for k, v := range head {
			fmt.Printf("%s: %s\n", k, v)
		}
	},
	"get": func(args []string) {
		if len(args) < 2 {
			fmt.Println("Too few arguments!")
			return
		}
		grp := args[0]
		id := args[1]
		var post client.Post
		err := state.conn.AsProtoStream().
			Get(grp, id, &post).
			Execute()

		if err != nil {
			fmt.Println("Error:", err.Error())
		}
		for k, v := range post.Headers {
			fmt.Printf("%s: %s\n", k, v)
		}
		fmt.Printf("\n%s\n", string(post.Contents))
	},
	"comp": func(args []string) {
		if len(args) != 4 {
			fmt.Println("Too few arguments!")
			return
		}

		var limit uint64
		var min, max int64
		group := args[0]
		fmt.Sscan(args[1], &min)
		fmt.Sscan(args[2], &max)
		fmt.Sscan(args[3], &limit)

		out := make([]client.CompositeField, 0)
		err := state.conn.AsProtoStream().
			Composite(group, time.Unix(min, 0), time.Unix(max, 0), limit, &out).
			Execute()
		if err != nil {
			fmt.Println("Error:", err.Error())
		}

		for _, field := range out {
			fmt.Println(field)
		}
	},
}

var state struct {
	conn *client.Conn
}

func main() {
	cert, _ := tls.LoadX509KeyPair("testdata/cert.pem", "testdata/key.pem")
	state.conn = client.NewConn(cert)

	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print("spdp> ")
		scanner.Scan()
		line := scanner.Text()
		runCommand(line)
	}
}

func runCommand(command string) {
	cmd := str.Split(command, " ")
	fn, ok := commands[cmd[0]]
	if !ok {
		fmt.Println("No such command!")
		return
	}

	fn(cmd[1:])
}
